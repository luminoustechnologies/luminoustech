//-----------------------------------------------------
// Design Name : Clock Generator
// File Name   : clock_gen.v
// Function    : clock divider
// Coder       : Eduardo H. Tozetto
//-----------------------------------------------------
module walking_one (

  clk, reset, enable


  );


input clk, reset, enable;





reg [31:0] count;

always @ (posedge clk)
if (reset == 1'b1) begin
  count <= 0;
end else if ( enable == 1'b1) begin
  count <= count + 1;
end


// Pattern Generator


endmodule
