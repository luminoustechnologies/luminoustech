//-----------------------------------------------------
// Design Name : LED Matrix - MODULE
// File Name   :
// Function    :
// Coder       : Eduardo H. Tozetto
//-----------------------------------------------------


// Parâmetros:
// Dimensões.

module led_matrix_module (
  spiclk,
  spidata,
  spilatch,
  line,
  LED
);

// parametrização do módulo
parameter pixels_width = 12;
parameter pixels_height = 12 ;
parameter leds_per_pixel = 3;


// LEDS_PER_CHIP = 3 ;
// LEDS_PER_CHIP  = 1 ;


/////////////// INOUT INTERFACE /////////////////////////////////////
input spiclk, spidata, spilatch;
input [pixels_height-1:0] line;
output [(pixels_width*leds_per_pixel*pixels_height):0] LED ;



//input spiclk ;
//output [35:0] LED_Light[31:0] ;
//output  LED_Light [31:0] ;
assign LED[0] = 1'b0 ;
//output [11:0] LED_Light [36:0];
reg  [31:0] LED_Light2[31:0] ;


endmodule
