module led_mod_ctrl_tb ( );


reg clock;
integer no_of_clocks;

parameter CLOCK_PERIOD = 5;
initial no_of_clocks = 0;
initial clock = 1'b0;

always #(CLOCK_PERIOD/2) clock = ~clock;

always@(posedge clock)
no_of_clocks = no_of_clocks +1 ;

initial
begin
#50000;
$display("End of simulation time is %d , total number of clocks seen is %d expected is %d",$time,no_of_clocks,($time/5));
$finish;
end










endmodule


