// Test Bench used to develop the LED Panel Controller - LPC system.
// autor: Eduardo H. Tozetto

// References:
// https://github.com/WillGreen/fpgatools
// https://timetoexplore.net/blog/initialize-memory-in-verilog

`timescale 1ns/1ns

//////////////////////////////////////////////////////////////////
module led_panel_controller_tb();

  // parametrização do test-bench
  parameter pixels_width = 12;
  parameter pixels_height = 12 ;
  parameter leds_per_pixel = 3;

  reg clk, reset, nrst , enable;
  wire [3:0] count;
  wire [11:0] line ;
  wire spi_clk ;




/////////// DUV
led_panel_controller led_panel_controller(
  .clk      (clk),
  .nrst     (nrst),
  .enable   (enable),
  .spi_clk  ( spi_clk ),
  .spi_out  (),
  .spi_latch(),
  .line     ( line )
  );



// LED MATRIX MODULE
led_matrix_module led_matrix_module  (

  .spiclk  ( spi_clk )


  );



/*

counter counter_instance1 (
  .clk    (clk),
  .reset  (reset),
  .enable (enable),
  .count  (count)
  );
*/




//////////////////////////////////////////////////////
  initial begin
    clk = 0;
    reset = 0;
    nrst = 1 ;
    enable = 0;

    #10 ;
    reset = 1;
    nrst = 0 ;
    #100 ;
    reset = 0;
    nrst = 1 ;


    #10 ;
    enable = 1 ;

  end

/*
requency =  50M Hz 	    Período =  20n
Frequency =  12.50M Hz  Período =  80n
Frequency =  10M Hz 	  Período =  100n
Frequency =  5k Hz 	    Período =  200u
*/

  always
    // 1/2 do período correspondente.
    #10 clk = ~clk;

  initial  begin
    $dumpfile ("led_panel_controller_tb.vcd");
    $dumpvars;
  end

  initial  begin
  //  $display("\t\ttime,\tclk,\treset,\tenable,\tcount");
  //  $monitor("%d,\t%b,\t%b,\t%b,\t%d",$time, clk,reset,enable,count);
  end


  initial begin
    #1000000000 $finish;
  end



endmodule





/*
/////////////////////////////////////
Code To run simulation
run_sim.sh
////////////////////////////////////

clear

vlog -work work  /home/eht/proj/modelsim/fpga_luminous/*.v \
	&& vsim -c led_panel_controller_tb -do run_sim.tcl \
	&& gconftool-2 --type string --set /com.geda.gtkwave/0/reload 0


gconftool-2 --type string --set /com.geda.gtkwave/0/zoom_size -- -10
gconftool-2 --type string --set /com.geda.gtkwave/0/move_to_time 1ns

/////////////////////////////////////
/// start_inotify.sh

/////////////////////////////////////

while inotifywait -e close_write *.v ; do ./run_sim.sh ; done

*/
