//-----------------------------------------------------
// Design Name : counter
// File Name   : counter.v
// Function    : 4 bit up counter
// Coder       : Deepak
//-----------------------------------------------------
module frame_buffer (clk, reset, enable, fb_red, fb_blue, fb_green);


input clk, reset, enable;
output [7:0] fb_red;
output [7:0] fb_blue;
output [7:0] fb_green;

reg [31:0] count;

always @ (posedge clk)
if (reset == 1'b1) begin
  count <= 0;
end else if ( enable == 1'b1) begin
  count <= count + 1;
end



// Pattern Generator









endmodule
