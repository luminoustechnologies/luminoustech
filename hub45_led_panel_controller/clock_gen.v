//-----------------------------------------------------
// Design Name : Clock Generator
// File Name   : clock_gen.v
// Function    : clock divider
// Coder       : Eduardo H. Tozetto
//-----------------------------------------------------
module clock_gen (
  clk,
  reset,
  enable,
  clock_walking_one,
  clock_spi
  );

input clk, reset, enable;
output clock_walking_one, clock_spi;

reg [31:0] count;
reg  clock_spi;
reg clock_walking_one;


localparam constantNumber = 2;
localparam spi_limit = 1;
localparam walking_one_limit = 1;

always @ (posedge(clk))
begin
    if (reset == 1'b1) begin
        count <= 32'b0;
        clock_spi <= 1'b0 ;
        clock_walking_one <= 1'b0 ;
        end
    else if (count ==  constantNumber ) begin
        count <= 32'b0;
        clock_spi <= ~clock_spi;
        end

    else
        count <= count +  32'b1;
end

always @ (posedge(clk))
begin
    if (reset == 1'b1)
        clock_spi <= 1'b0;
    else if (count == constantNumber )
        clock_spi <= ~clock_spi;
    else
        clock_spi <= clock_spi;
end


endmodule
