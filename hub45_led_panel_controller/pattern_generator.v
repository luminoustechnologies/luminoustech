//-----------------------------------------------------
// Design Name : counter
// File Name   : counter.v
// Function    : 4 bit up counter
// Coder       : Deepak
//-----------------------------------------------------
module pattern_generator (

  clk, reset, enable, spi_out


  );


input clk, reset, enable;
output spi_out;

reg [31:0] count;

always @ (posedge clk)
if (reset == 1'b1) begin
  count <= 0;
end else if ( enable == 1'b1) begin
  count <= count + 1;
end


// Pattern Generator


endmodule
