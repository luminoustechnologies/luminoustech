//-----------------------------------------------------
// Design Name : LED Panel Controller
// File Name   : led_panel_controller.v
// Function    : Controlador de módulos usados para implementar painéis de LED.
// Coder       : Eduardo H. Tozetto
// Luminous Technologies.
//-----------------------------------------------------
module led_panel_controller (

clk,
nrst,
enable,
spi_clk,
spi_out,
spi_latch,
line,
led

);



input clk, nrst, enable;
output spi_clk, spi_out, spi_latch;
output [11:0] line ;


parameter pixels_width = 12;
parameter pixels_height = 12 ;
parameter pixel_type = 1;
// type 1 = RGB
// type 2 = monochromatic.

/*
parameter number_of_columns = 36;
parameter number_of_lines = 12;
*/
parameter number_of_columns = 3;
parameter number_of_lines = 1;


wire spi_clk ;





reg  frame_matrix [0:pixels_height-1][0:(pixels_width*3)-1];

assign reset = ~nrst;
//assign clock_spi =

reg [3:0] count;


// onboard leds
reg [3:0] led_reg;

//reg  [7:0] frame_01 [0:number_of_lines-1][0:number_of_columns-1] =   { 8'haa, 8'hbb, 8'hcc };
reg  frame [0:number_of_lines-1][0:number_of_columns-1];
reg  [7:0] y3 [0:1][0:3];
reg  y4 [0:1][0:3];


initial begin
 y4[1][2] = 2'b11;
 y3[1][2] = 8'hdd;   // Assign 0xdd to rows=1 cols=2
 y3[0][0] = 8'haa;   // Assign 0xaa to rows=0 cols=0

end

reg [15:0] register;

inout [3:0]led;



/////// Instances /////////////////////////////

clock_gen clock_gen(
  .clk    (clk),
  .reset  (reset),
  .enable (enable),
  .clock_spi ( spi_clk),
  .clock_walking_one()
  );

frame_buffer  frame_buffer(   );

pattern_generator pattern_generator(
  .clk    (clk),
  .reset  (reset),
  .enable (enable)
     );

walking_one walking_one (
  .clk    (clk),
  .reset  (reset),
  .enable (enable)
    );






endmodule
